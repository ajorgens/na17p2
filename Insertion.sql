INSERT INTO Fichier VALUES ('Photo01.jpg');
INSERT INTO Fichier VALUES ('Photo02.jpg');
INSERT INTO Fichier VALUES ('Photo03.jpg');
INSERT INTO Fichier VALUES ('Photo04.jpg');
INSERT INTO Fichier VALUES ('Photo05.jpg');
INSERT INTO Fichier VALUES ('Photo06.jpg');
INSERT INTO Fichier VALUES ('Photo07.jpg');
INSERT INTO Fichier VALUES ('Photo08.jpg');
INSERT INTO Fichier VALUES ('Photo09.jpg');
INSERT INTO Fichier VALUES ('Photo10.jpg');
INSERT INTO Fichier VALUES ('Video01.mp4');
INSERT INTO Fichier VALUES ('Video02.mp4');
INSERT INTO Fichier VALUES ('Video03.mp4');
INSERT INTO Fichier VALUES ('Schema01.svg');

INSERT INTO Personne VALUES (1, 'Jorgensen', 'Alexandre', '0601020304', 'alexandre.jorgensen@etu.utc.fr', 'Photo01.jpg');
INSERT INTO Personne VALUES (2, 'Jorgensen', 'Olivier', '0123456789', 'olivier.jorgensen@etu.utc.fr', 'Photo02.jpg');
INSERT INTO Personne VALUES (3, 'Claude', 'Jean', '0987654321', 'jean.claude@etu.utc.fr', 'Photo03.jpg');
INSERT INTO Personne VALUES (4, 'Linou', 'Michelle', '0698765432', 'michelle.linou@etu.utc.fr', 'Photo04.jpg');
INSERT INTO Personne VALUES (5, 'Yrat', 'Sam', '0132547698', 'sam.yrat@etu.utc.fr', 'Photo05.jpg');

INSERT INTO Karateka (id, poids, taille, ceinture) VALUES (1, 65, 176, 'M');
INSERT INTO Karateka (id, poids, taille, ceinture, dans) VALUES (3, 75, 178, 'N', 4);
INSERT INTO Karateka (id, poids, taille, ceinture) VALUES (4, 55, 167, 'J');
INSERT INTO Karateka (id, poids, taille, ceinture) VALUES (5, 63, 174, 'VB');

INSERT INTO Categorie VALUES ('deplacement');
INSERT INTO Categorie VALUES ('defense');
INSERT INTO Categorie VALUES ('technique de bras');
INSERT INTO Categorie VALUES ('technique de jambes');

INSERT INTO Sous_Categorie VALUES ('poings');
INSERT INTO Sous_Categorie VALUES ('coudes');
INSERT INTO Sous_Categorie VALUES ('pieds');
INSERT INTO Sous_Categorie VALUES ('genoux');
INSERT INTO Sous_Categorie VALUES ('positions');
INSERT INTO Sous_Categorie VALUES ('projections');
INSERT INTO Sous_Categorie VALUES ('cles');

INSERT INTO Famille_Kata VALUES ('Heian', 'Paix Tranquille');
INSERT INTO Famille_Kata VALUES ('Taikyoku', 'Cause Ultime');
INSERT INTO Famille_Kata VALUES ('Tekki', 'Guerrier arme chevauchant son cheval');

INSERT INTO Mouvement VALUES ('Hidari Gedan-barai', 'Garde a gauche');
INSERT INTO Mouvement VALUES ('Migi Chudan Oï-zuki', 'Coup de point droit');
INSERT INTO Mouvement VALUES ('Migi Gedan-barai', 'Coup de point crochet');

INSERT INTO Kata (nom_japonais, traduction, description, nombre_mouvement, ceinture_necessaire, famille_kata, schema) VALUES ('Heian Sho Dan', 'Esprit Paisible Premier Niveau', 'Heian Shodan était à l''origine une pratique orthodoxe et devrait être conservé exactement comme il était censé être. C''est la forme originale de Shaolin, notre pratique préférée en matière de Kata. C''est un Kata très authentique et chaque mouvement doit être fait et exprimé avec un grand dynamisme et un sentiment propre.', 3, 'J', 'Heian', 'Schema01.svg');

INSERT INTO Kata_Categorie VALUES ('Heian Sho Dan', 'deplacement');
INSERT INTO Kata_Categorie VALUES ('Heian Sho Dan', 'defense');

INSERT INTO Kata_Sous_Categorie VALUES ('Heian Sho Dan', 'poings');
INSERT INTO Kata_Sous_Categorie VALUES ('Heian Sho Dan', 'coudes');

INSERT INTO Kata_Video VALUES ('Heian Sho Dan', 'Video01.mp4');
INSERT INTO Kata_Video VALUES ('Heian Sho Dan', 'Video02.mp4');
INSERT INTO Kata_Video VALUES ('Heian Sho Dan', 'Video03.mp4');

INSERT INTO Code_postal VALUES('60200', 'Compiegne');

INSERT INTO Lieu VALUES (1, 'Dojo Maitre Pin', '12 rue des karatekas', '60200');
INSERT INTO Lieu VALUES (2, 'Stade Robert Desnos', '128 avenue des petits pois', '60200');

INSERT INTO Club VALUES (1, 'Tigres de la paix', 'www.tigre_de_la_paix.fr', 2);
INSERT INTO Club VALUES (2, 'Lion de l''harmonie', 'www.lionharmonie.fr', 3);

INSERT INTO Enseigne VALUES (1,1);
INSERT INTO Enseigne VALUES (3,1);
INSERT INTO Enseigne VALUES (3,2);

INSERT INTO Etudie VALUES (4,1);
INSERT INTO Etudie VALUES (1,2);
INSERT INTO Etudie VALUES (5,2);

INSERT INTO Maitrise VALUES (1, 'Heian Sho Dan');
INSERT INTO Maitrise VALUES (3, 'Heian Sho Dan');
INSERT INTO Maitrise VALUES (5, 'Heian Sho Dan');

INSERT INTO Competition VALUES (1, 'Compétition annuelle de karaté de Compiègne - Edition 2020', 'mixte', '07-02-30 12:30', '08-02-30 18:30', 'www.agglo-compiegne.fr/competition_karate', 'karate@compiegne.fr', 1, 2);

INSERT INTO Competition_Interdis VALUES (1, 'cles');
INSERT INTO Competition_Interdis VALUES (1, 'projections');

INSERT INTO Competition_Authorise VALUES (1, 'poings', 1);
INSERT INTO Competition_Authorise VALUES (1, 'coudes', 3);
INSERT INTO Competition_Authorise VALUES (1, 'pieds', 2);

INSERT INTO Participant VALUES (1, 1, 1);
INSERT INTO Participant VALUES (1, 3, 2);
INSERT INTO Participant VALUES (1, 4, 3);
INSERT INTO Participant VALUES (1, 5, NULL);

INSERT INTO Photo_Competition VALUES (1, 'Photo06.jpg');
INSERT INTO Photo_Competition VALUES (1, 'Photo07.jpg');
INSERT INTO Photo_Competition VALUES (1, 'Photo08.jpg');
INSERT INTO Photo_Competition VALUES (1, 'Photo09.jpg');
INSERT INTO Photo_Competition VALUES (1, 'Photo10.jpg');

INSERT INTO Confronte VALUES (1, 1, 'kata', 1, 3, 24, 12, 'Heian Sho Dan');
INSERT INTO Confronte VALUES (1, 2, 'kumite', 1, 4, 2, 5);

INSERT INTO Coup VALUES (1, 2, 1, 1, 1);
INSERT INTO Coup VALUES (1, 2, 1, 2, 1);
INSERT INTO Coup VALUES (1, 2, 4, 1, 3);
INSERT INTO Coup VALUES (1, 2, 4, 2, 2);