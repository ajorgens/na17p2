# Objets :

##### Objet > Personne > Karateka
- nom (non nul)
- prenom (non nul)
- age (non nul)
- telephone (hypothese, non nul)
- mail (hypothese, non nul)
- possède 1 photo (Fichier)
- Karateka :
    - poids (non nul)
    - taille (non nul)
    - ceinture (non nul) {"J", "JO", "O", "OV", "V", "VB", "B", "BM", "M", "N"}
    - dans (nomnbre)
    - maitrise N Kata

##### Objet > Lieu
- nom (hypothèse, non nul)
- adresse (non nul)
- possède 1 Code_postal

##### Objet > Code_postal
- code_postal (non nul)
- ville (non nul)

##### Objet > Club
- nom (non nul)
- est dirigé par 1 personne
- siteWeb
- possède N professeurs (Karateka)
- possède N elèves (Karateka)

##### Objet > Compétition
- nom (non nul)
- type (non nul) {"kumite", "kata", "tameshi wari", "mixte"}
- date_debut (non nul, type date et heure)
- date_fin(non nul, type date et heure)
- est organisé par 1 Club
- se situe à 1 lieu
- siteWeb (non nul)
- mailContact (non nul)
- oppose N karateka avec un classement assigné à celui-ci (peut être nul)
- possede N confrontation
- possede N photo (Fichier)
- Compétition_Kumite :
    - interdit N Catégorie
    - attribue un nombre de point à N catégorie

##### Objet > Confronte > Confrontation_Kumite, Confrontation_Kata
- oppose 2 Karateka (non nul) avec un score assigné à ceux-ci
- Confrontation_Kumite :
    - Possède N coup
- Confrontation_Kata :
    - correspond à 1 Kata

##### Objet > Kata
- nom_japonais (unique, on nul)
- traduction (unique, non nul)
- description (non nul)
- possède 1 ou N Video_Explication (Fichier)
- possède 1 schema (Fichier)
- possède une liste ordonné de N Mouvement
- nombreMouvement (non nul)
- ceinture_necessaire (non nul) {"J", "JO", "O", "OV", "V", "VB", "B", "BM", "M", "N"}
- dans_necessaire 
- appartient à 1 Famille_Kata

##### Objet > Mouvement
- nom_japonais (unique, non nul)
- traduction (unique, non nul)
- possède 1 schema (Fichier)
- appartient N Catégorie
- appartient N Sous_Catégorie

##### Objet > Catégorie
- nom (unique, non nul)

##### Objet > Sous_Catégorie
- nom (unique, non nul)

##### Objet > Famille_Kata
- nom_japonais (unique, non nul)
- traduction (unique, non nul)

##### Objet > Coup
- numero : entier positif (non nul)
- donner par 1 Karateka

##### Objet > Fichier
- adresse (unique, non nul)

# Contraintes :
- C0 : Un karateka, un kata, ... possède un nombre de dans que si il est ceinture noir
- C1 : La base doit pouvoir être initialisé avec tous les katas, mouvements et grades du karaté (à partir d'un fichier CSV ou XML)
- C2 : Les compétitions de type kumite doivent être reliés exclusivement aux confrontations de kumite
- C3 : Les compétitions de type kata doivent être reliés exclusivement aux confrontations de kata
- C4 : Les compétitions de type tameshi wari doivent être reliés exclusivement aux confrontations sans type particulier

# Rôle :
Un seul rôle a été clairement disserné (celui d'administrateur)

##### Un administateur peut :
- Créer et modifier un karatéka
- Créer et modifier une personne
- Créer et modifier un club
- Créer et modifier une compétition
- Inscrire ou désinscrire un karatéka à un club
- Inscrire ou désinscrire un karatéka à une compétition
- Créer et saisir le résultats des confrontations
- Obtenir la liste des liste des confrontations er résultats (classement par karatéka et par club) d'une compétition en cours ou terminée
- Obtenir le descriptif complet d'un karatéka (avec toutes ses compétitions et confrontations)


# Hypothèses :
- H0 : On suppose que les photos, vidéos et autres fichiers lourds seront stockés sur le serveur en dehors de la base de donnée. La base de donnée contiendra un lien vers l'emplacement de ces fichiers (commentaire : intéressant de le dire à la phase de ndc ?). Ces liens seront stockés dans l'objet Fichier
- H1 : On suppose que tous les profs de karaté sont des karatékas
- H2 : On ne précise pas le type de compétition de tameshi wari