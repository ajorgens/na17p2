DROP TABLE IF EXISTS Coup;
DROP TABLE IF EXISTS Confronte;
DROP TABLE IF EXISTS Photo_Competition;
DROP TABLE IF EXISTS Participant;
DROP TABLE IF EXISTS Competition_Authorise;
DROP TABLE IF EXISTS Competition_Interdis;
DROP TABLE IF EXISTS Competition;
DROP TABLE IF EXISTS Maitrise;
DROP TABLE IF EXISTS Etudie;
DROP TABLE IF EXISTS Enseigne;
DROP TABLE IF EXISTS Club;
DROP TABLE IF EXISTS Lieu;
DROP TABLE IF EXISTS List_Mouvement;
DROP TABLE IF EXISTS Kata_Video;
DROP TABLE IF EXISTS Kata_Sous_Categorie;
DROP TABLE IF EXISTS Kata_Categorie;
DROP TABLE IF EXISTS Kata;
DROP TABLE IF EXISTS Mouvement;
DROP TABLE IF EXISTS Famille_Kata;
DROP TABLE IF EXISTS Sous_Categorie;
DROP TABLE IF EXISTS Categorie;
DROP TABLE IF EXISTS Karateka;
DROP TABLE IF EXISTS Personne;
DROP TABLE IF EXISTS Fichier;

CREATE TABLE Fichier(
    emplacement VARCHAR PRIMARY KEY
);

CREATE TABLE Personne(
    id INTEGER PRIMARY KEY,
    nom VARCHAR(20) NOT NULL,
    prenom VARCHAR(20) NOT NULL,
    telephone VARCHAR(10) NOT NULL,
    mail VARCHAR(100) NOT NULL,
    photo VARCHAR NOT NULL,
    FOREIGN KEY (photo) REFERENCES Fichier(emplacement)
);

CREATE TABLE Karateka(
    id INTEGER PRIMARY KEY,
    poids INTEGER NOT NULL,
    taille INTEGER NOT NULL,
    ceinture VARCHAR(2) NOT NULL,
    dans INTEGER,
    FOREIGN KEY (id) REFERENCES Personne(id)
);

CREATE TABLE Categorie(
    nom VARCHAR PRIMARY KEY
);

CREATE TABLE Sous_Categorie(
    nom VARCHAR PRIMARY KEY
);

CREATE TABLE Famille_Kata(
    nom_japonais VARCHAR PRIMARY KEY,
    traduction VARCHAR NOT NULL UNIQUE
);

CREATE TABLE Mouvement(
    nom_japonais VARCHAR PRIMARY KEY,
    traduction VARCHAR NOT NULL UNIQUE
);

CREATE TABLE Kata(
    nom_japonais VARCHAR PRIMARY KEY,
    traduction VARCHAR NOT NULL UNIQUE,
    description VARCHAR NOT NULL,
    nombre_mouvement INTEGER NOT NULL,
    ceinture_necessaire VARCHAR NOT NULL,
    dans_necessaire INTEGER,
    famille_kata VARCHAR,
    schema VARCHAR,
    FOREIGN KEY (famille_kata) REFERENCES Famille_Kata(nom_japonais),
    FOREIGN KEY (schema) REFERENCES Fichier(emplacement)
);

CREATE TABLE Kata_Categorie(
    kata VARCHAR,
    categorie VARCHAR,
    PRIMARY KEY (kata, categorie),
    FOREIGN KEY (kata) REFERENCES Kata(nom_japonais),
    FOREIGN KEY (categorie) REFERENCES Categorie(nom)
);

CREATE TABLE Kata_Sous_Categorie(
    kata VARCHAR,
    sous_categorie VARCHAR,
    PRIMARY KEY (kata, sous_categorie),
    FOREIGN KEY (kata) REFERENCES Kata(nom_japonais),
    FOREIGN KEY (sous_categorie) REFERENCES Sous_Categorie(nom)
);

CREATE TABLE Kata_Video(
    kata VARCHAR,
    video VARCHAR,
    PRIMARY KEY (kata, video),
    FOREIGN KEY (kata) REFERENCES Kata(nom_japonais),
    FOREIGN KEY (video) REFERENCES Fichier(emplacement)
);

CREATE TABLE List_Mouvement(
    kata VARCHAR,
    numero INTEGER,
    mouvement VARCHAR NOT NULL,
    PRIMARY KEY (kata, numero),
    FOREIGN KEY (kata) REFERENCES Kata(nom_japonais),
    FOREIGN KEY (mouvement) REFERENCES Mouvement(nom_japonais)
);

CREATE TABLE Code_postal(
    code_postal VARCHAR PRIMARY KEY,
    ville VARCHAR
)

CREATE TABLE Lieu(
    id INTEGER PRIMARY KEY,
    nom VARCHAR NOT NULL,
    adresse VARCHAR NOT NULL,
    code_postal VARCHAR,
    FOREIGN KEY (code_postal) REFERENCES Code_postal(code_postal)
);

CREATE TABLE Club(
    id INTEGER PRIMARY KEY,
    nom VARCHAR NOT NULL,
    site_web VARCHAR NOT NULL,
    dirigeant INTEGER NOT NULL,
    FOREIGN KEY (dirigeant) REFERENCES Personne(id)
);

CREATE TABLE Enseigne(
    enseignant INTEGER,
    club INTEGER,
    PRIMARY KEY (enseignant, club),
    FOREIGN KEY (enseignant) REFERENCES Karateka(id),
    FOREIGN KEY (club) REFERENCES Club(id)
);

CREATE TABLE Etudie(
    eleve INTEGER,
    club INTEGER,
    PRIMARY KEY (eleve, club),
    FOREIGN KEY (eleve) REFERENCES Karateka(id),
    FOREIGN KEY (club) REFERENCES Club(id)
);

CREATE TABLE Maitrise(
    karateka INTEGER,
    kata VARCHAR,
    PRIMARY KEY (karateka, kata),
    FOREIGN KEY (karateka) REFERENCES Karateka(id),
    FOREIGN KEY (kata) REFERENCES Kata(nom_japonais)
);

CREATE TABLE Competition(
    id INTEGER PRIMARY KEY,
    nom VARCHAR NOT NULL,
    type VARCHAR NOT NULL,
    date_debut TIMESTAMP NOT NULL,
    date_fin TIMESTAMP NOT NULL,
    siteweb VARCHAR NOT NULL,
    mail_contact VARCHAR NOT NULL,
    organise INTEGER,
    lieu INTEGER,
    FOREIGN KEY (organise) REFERENCES Club(id),
    FOREIGN KEY (lieu) REFERENCES Lieu(id)
);

CREATE TABLE Competition_Interdis(
    competition INTEGER,
    sous_categorie VARCHAR,
    PRIMARY KEY (competition, sous_categorie),
    FOREIGN KEY (competition) REFERENCES Competition(id),
    FOREIGN KEY (sous_categorie) REFERENCES Sous_Categorie(nom)
);

CREATE TABLE Competition_Authorise(
    competition INTEGER,
    sous_categorie VARCHAR,
    nb_points INTEGER NOT NULL,
    PRIMARY KEY (competition, sous_categorie),
    FOREIGN KEY (competition) REFERENCES Competition(id),
    FOREIGN KEY (sous_categorie) REFERENCES Sous_Categorie(nom)
);

CREATE TABLE Participant(
    competition INTEGER,
    karateka INTEGER,
    classement INTEGER,
    PRIMARY KEY (competition, karateka),
    FOREIGN KEY (competition) REFERENCES Competition(id),
    FOREIGN KEY (karateka) REFERENCES Karateka(id)
);

CREATE TABLE Photo_Competition(
    competition INTEGER,
    photo VARCHAR,
    PRIMARY KEY (competition, photo),
    FOREIGN KEY (competition) REFERENCES Competition(id),
    FOREIGN KEY (photo) REFERENCES Fichier(emplacement)
);

CREATE TABLE Confronte(
    competition INTEGER,
    numero INTEGER,
    type VARCHAR NOT NULL,
    karateka1 INTEGER NOT NULL,
    karateka2 INTEGER NOT NULL,
    score_karateka1 INTEGER,
    score_karateka2 INTEGER,
    kata VARCHAR,
    PRIMARY KEY (competition, numero),
    FOREIGN KEY (competition) REFERENCES Competition(id),
    FOREIGN KEY (karateka1) REFERENCES Karateka(id),
    FOREIGN KEY (karateka2) REFERENCES Karateka(id)
);

CREATE TABLE Coup(
    confronte_competition INTEGER,
    confronte_numero INTEGER,
    karateka INTEGER,
    numero INTEGER,
    nb_point INTEGER NOT NULL,
    PRIMARY KEY (confronte_competition, confronte_numero, karateka, numero),
    FOREIGN KEY (confronte_competition, confronte_numero) REFERENCES Confronte(competition, numero),
    FOREIGN KEY (karateka) REFERENCES Karateka(id)
);


/*
CREATE TRIGGER trKarateka
BEFORE INSERT OR UPDATE
ON Karateka
FOR EACH ROW
BEGIN
    IF NOT(new.ceinture IN ("J", "JO", "O", "OV", "V", "BV", "B", "BM", "M", "N") OR (new.ceinture = "N" AND new.dans = NULL)) THEN
        new.id = NULL;
    END IF;
END;
/

CREATE TRIGGER trKata
BEFORE INSERT OR UPDATE
ON Kata
FOR EACH ROW
BEGIN
    IF NOT(new.ceinture_necessaire IN ("J", "JO", "O", "OV", "V", "BV", "B", "BM", "M", "N") OR (new.ceinture_necessaire = "N" AND new.dans_necessaire = NULL)) THEN
        new.nom_japonais = NULL;
    END IF;
END;
/

CREATE TRIGGER trConfronte
BEFORE INSERT OR UPDATE
ON Confronte
FOR EACH ROW
BEGIN
    IF NOT(new.type IN ("kumite", "kata", "tameshi wari")) THEN
        new.competition = NULL;
    END IF;
    IF new.karateka1 = new.karateka2 THEN
        new.competition = NULL;
    END IF;
    IF new.type = "kata" AND kata = NULL OR new.type <> "kata" AND kata <> NULL THEN
        new.competition = NULL;
    END IF;
END;
/

CREATE TRIGGER trCompetition
BEFORE INSERT OR UPDATE
ON Competition
FOR EACH ROW
BEGIN
    IF NOT(new.type IN ("kumite", "kata", "tameshi wari", "mixte")) THEN
        new.competition = NULL;
    END IF;
END;
/

*/