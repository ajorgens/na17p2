-- Liste de toutes les confrontations et résultats par karateka d'une compétition
SELECT Comp.nom AS Competition, K.id as id_karateka, K.nom as nom_karateka, K.prenom as prenom_karateka, Part.classement AS classement, Kopp.nom AS nom_adv, Kopp.prenom AS prenom_adv, Conf.score_karateka1 AS score_perso, Conf.score_karateka2 AS score_adv
FROM Competition Comp
INNER JOIN Participant Part On Comp.id = Part.competition
INNER JOIN Personne K ON Part.karateka = K.id
INNER JOIN Confronte Conf ON Comp.id = Conf.competition AND K.id = Conf.karateka1 
INNER JOIN Personne Kopp ON Conf.karateka2 = Kopp.id
WHERE Comp.id = 1
UNION
SELECT Comp.nom AS Competition, K.id as id_karateka, K.nom as nom_karateka, K.prenom as prenom_karateka, Part.classement AS classement, Kopp.nom AS nom_adv, Kopp.prenom AS prenom_adv, Conf.score_karateka1 AS score_perso, Conf.score_karateka2 AS score_adv
FROM Competition Comp
INNER JOIN Participant Part On Comp.id = Part.competition
INNER JOIN Personne K ON Part.karateka = K.id
INNER JOIN Confronte Conf ON Comp.id = Conf.competition AND K.id = Conf.karateka2 
INNER JOIN Personne Kopp ON Conf.karateka1 = Kopp.id
WHERE Comp.id = 1
ORDER BY (id_karateka);
-- Etant donnee qu'un karateka peut appartenir à plusieurs clubs, on ne classera pas par club


-- Descriptif complet d'un karatéka
SELECT K.id AS id, P.nom AS nom, P.prenom AS prenom, P.telephone AS tel, P.mail AS mail, P.photo AS photo, K.taille AS taille, K.poids AS poids, K.ceinture AS ceinture, K.dans AS dans, C.nom AS etudie_a, C2.nom AS enseigne_a, Comp.nom AS participe, Part.classement AS classement, Kopp1.nom AS nom_adv, Kopp1.prenom AS prenom_adv, Conf1.score_karateka1 AS score_perso, Conf1.score_karateka2 AS score_adv, Kopp2.nom AS nom_adv, Kopp2.prenom AS prenom_adv, Conf2.score_karateka2 AS score_perso, Conf2.score_karateka1 AS score_adv
FROM Karateka K 
INNER JOIN Personne P ON K.id = P.id 
LEFT JOIN Maitrise M on M.karateka = K.id 
LEFT JOIN Etudie Ele ON K.id = Ele.eleve 
LEFT JOIN Club C ON Ele.club = C.id 
LEFT JOIN Enseigne Ens ON K.id = Ens.enseignant 
LEFT JOIN Club C2 ON Ens.club = C2.id 
LEFT JOIN Participant Part On K.id = Part.karateka 
LEFT JOIN Competition Comp ON Part.competition = Comp.id 
LEFT JOIN Confronte Conf1 ON Comp.id = Conf1.competition AND K.id = Conf1.karateka1 
LEFT JOIN Personne Kopp1 ON Conf1.karateka2 = Kopp1.id
LEFT JOIN Confronte Conf2 ON Comp.id = Conf2.competition AND K.id = Conf2.karateka2
LEFT JOIN Personne Kopp2 ON Conf2.karateka1 = Kopp2.id
WHERE K.id = 1;

-- Détail du karateka
SELECT K.id AS id, P.nom AS nom, P.prenom AS prenom, P.telephone AS tel, P.mail AS mail, P.photo AS photo, K.taille AS taille, K.poids AS poids, K.ceinture AS ceinture, K.dans AS dans
FROM Karateka K 
INNER JOIN Personne P ON K.id = P.id
WHERE K.id = 1;

-- Clubs auquel il appartient
SELECT K.id AS id, P.nom AS nom, P.prenom AS prenom, C.nom AS etudie_a, C2.nom AS enseigne_a
FROM Karateka K 
INNER JOIN Personne P ON K.id = P.id
LEFT JOIN Etudie Ele ON K.id = Ele.eleve 
LEFT JOIN Club C ON Ele.club = C.id 
LEFT JOIN Enseigne Ens ON K.id = Ens.enseignant 
LEFT JOIN Club C2 ON Ens.club = C2.id
WHERE K.id = 1;

-- Competitions effectuees et confrontation
SELECT K.id AS id, P.nom AS nom, P.prenom AS prenom, Comp.nom AS participe, Part.classement AS classement, Kopp1.nom AS nom_adv, Kopp1.prenom AS prenom_adv, Conf1.score_karateka1 AS score_perso, Conf1.score_karateka2 AS score_adv
FROM Karateka K 
INNER JOIN Personne P ON K.id = P.id
INNER JOIN Participant Part On K.id = Part.karateka 
INNER JOIN Competition Comp ON Part.competition = Comp.id 
LEFT JOIN Confronte Conf1 ON Comp.id = Conf1.competition AND K.id = Conf1.karateka1 
LEFT JOIN Personne Kopp1 ON Conf1.karateka2 = Kopp1.id 
WHERE K.id = 1
UNION
SELECT K.id AS id, P.nom AS nom, P.prenom AS prenom, Comp.nom AS participe, Part.classement AS classement, Kopp2.nom AS nom_adv, Kopp2.prenom AS prenom_adv, Conf2.score_karateka2 AS score_perso, Conf2.score_karateka1 AS score_adv
FROM Karateka K 
INNER JOIN Personne P ON K.id = P.id
INNER JOIN Participant Part On K.id = Part.karateka 
INNER JOIN Competition Comp ON Part.competition = Comp.id 
LEFT JOIN Confronte Conf2 ON Comp.id = Conf2.competition AND K.id = Conf2.karateka2
LEFT JOIN Personne Kopp2 ON Conf2.karateka1 = Kopp2.id
WHERE K.id = 1;